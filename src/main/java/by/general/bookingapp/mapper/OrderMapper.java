package by.general.bookingapp.mapper;

import by.general.bookingapp.dto.order.OrderCreateDto;
import by.general.bookingapp.dto.order.OrderFullDto;
import by.general.bookingapp.entity.OrderEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface OrderMapper {

    @Mapping(target = "userId", expression = "java(entity.getUser().getId())")
    @Mapping(target = "roomId", expression = "java(entity.getRoom().getId())")
    OrderFullDto map (OrderEntity entity);

    OrderEntity map (OrderCreateDto dto);

    List<OrderFullDto> map (List <OrderEntity> entities);


}

