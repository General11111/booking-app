package by.general.bookingapp.mapper;

import by.general.bookingapp.dto.user.UserCreateDto;
import by.general.bookingapp.dto.user.UserFullDto;
import by.general.bookingapp.dto.user.UserShortDto;
import by.general.bookingapp.entity.UserEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", uses = OrderMapper.class)
public interface UserMapper {

    UserFullDto map(UserEntity entity);

    UserEntity map(UserCreateDto dto);

    List<UserShortDto> map(List<UserEntity> entities);

}
