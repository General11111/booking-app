package by.general.bookingapp.mapper;

import by.general.bookingapp.dto.room.RoomCreateDto;
import by.general.bookingapp.dto.room.RoomFullDto;
import by.general.bookingapp.dto.room.RoomShortDto;
import by.general.bookingapp.entity.RoomEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", uses = OrderMapper.class)
public interface RoomMapper {

    RoomFullDto map (RoomEntity entity);

    RoomEntity map (RoomCreateDto dto);

    List<RoomShortDto> map (List<RoomEntity> entities);


}
