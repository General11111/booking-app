package by.general.bookingapp.mapper;

import by.general.bookingapp.dto.hotel.HotelCreateDto;
import by.general.bookingapp.dto.hotel.HotelFullDto;
import by.general.bookingapp.dto.hotel.HotelShortDto;
import by.general.bookingapp.entity.HotelEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", uses = RoomMapper.class)
public interface HotelMapper {

    @Mapping(target = "countryId", expression = "java(entity.getCountry().getId())")
    HotelFullDto map (HotelEntity entity);

    HotelEntity map (HotelCreateDto dto);

    List<HotelShortDto> map (List<HotelEntity> entities);


}
