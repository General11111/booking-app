package by.general.bookingapp.mapper;

import by.general.bookingapp.dto.country.CountryCreateDto;
import by.general.bookingapp.dto.country.CountryFullDto;
import by.general.bookingapp.entity.CountryEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", uses = HotelMapper.class)
public interface CountryMapper {

    CountryFullDto map (CountryEntity entity);

    CountryEntity map (CountryCreateDto dto);

    List<CountryFullDto> map (List <CountryEntity> entities);



}
