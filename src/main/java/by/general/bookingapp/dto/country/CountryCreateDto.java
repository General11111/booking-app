package by.general.bookingapp.dto.country;

import by.general.bookingapp.dto.hotel.HotelCreateDto;
import by.general.bookingapp.dto.hotel.HotelShortDto;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class CountryCreateDto {

    private String name;

}
