package by.general.bookingapp.dto.country;

import by.general.bookingapp.dto.hotel.HotelShortDto;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class CountryUpdateDto {

    private Integer id;

    private String name;

    private List<HotelShortDto> hotels = new ArrayList<>();

}
