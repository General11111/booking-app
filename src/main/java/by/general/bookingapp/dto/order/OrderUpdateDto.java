package by.general.bookingapp.dto.order;

import lombok.Data;

import java.sql.Date;

@Data
public class OrderUpdateDto {

    private Integer id;

    private Date dateIn;

    private Date dateOut;

    private Double cost;

    private Integer roomId;

    private Integer userId;

}
