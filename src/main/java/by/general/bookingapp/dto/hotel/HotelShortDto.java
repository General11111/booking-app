package by.general.bookingapp.dto.hotel;

import lombok.Data;

@Data
public class HotelShortDto {

    private Integer id;

    private String name;

    private String address;

    private Integer numberOfStars;

    private Integer countryId;

}
