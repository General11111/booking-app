package by.general.bookingapp.dto.hotel;

import by.general.bookingapp.dto.room.RoomShortDto;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class HotelUpdateDto {

    private Integer id;

    private String name;

    private Integer numberOfStars;

}
