package by.general.bookingapp.dto.hotel;

import by.general.bookingapp.dto.room.RoomCreateDto;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
public class HotelCreateDto {

    @NotBlank
    private String name;

    @NotBlank
    private String address;

    @NotBlank
    private Integer numberOfStars;

    @NotEmpty
    private Integer countryId;

    private List<RoomCreateDto> rooms;


}
