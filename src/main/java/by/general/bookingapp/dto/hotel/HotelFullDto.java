package by.general.bookingapp.dto.hotel;

import by.general.bookingapp.dto.room.RoomShortDto;
import lombok.Data;
import java.util.ArrayList;
import java.util.List;

@Data
public class HotelFullDto {

    private Integer id;

    private String name;

    private String address;

    private Integer numberOfStars;

    private Integer countryId;

    private List<RoomShortDto> rooms = new ArrayList<>();


}
