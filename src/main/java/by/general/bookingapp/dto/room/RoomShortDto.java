package by.general.bookingapp.dto.room;

import lombok.Data;

@Data
public class RoomShortDto {

    private Integer id;

    private Integer numberOfPlaces;

    private Integer hotelId;


}
