package by.general.bookingapp.dto.room;

import by.general.bookingapp.dto.order.OrderFullDto;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class RoomFullDto {

    private Integer id;

    private Integer numberOfPlaces;

    private Integer hotelId;

    private List<OrderFullDto> orders = new ArrayList<>();

}
