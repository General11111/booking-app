package by.general.bookingapp.dto.user;

import by.general.bookingapp.dto.order.OrderFullDto;
import by.general.bookingapp.entity.UserRole;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class UserFullDto {

    private Integer id;

    private String firstName;

    private String lastName;

    private String phone;

    private String email;

    private UserRole role;

    private Double amount;

    private List<OrderFullDto> orders = new ArrayList<>();

}
