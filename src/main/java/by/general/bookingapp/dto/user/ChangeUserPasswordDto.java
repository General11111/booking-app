package by.general.bookingapp.dto.user;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class ChangeUserPasswordDto {

    @NotNull
    private Integer id;

    @NotNull
    private String oldPassword;

    @NotBlank
    @Size(min = 8)
    private String newPassword;

}
