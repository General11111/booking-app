package by.general.bookingapp.dto.user;

import by.general.bookingapp.entity.UserRole;
import lombok.Data;
import javax.validation.constraints.NotNull;


@Data
public class ChangeUserRoleDto {

    @NotNull
    private Integer id;

    @NotNull
    private UserRole newRole;

}
