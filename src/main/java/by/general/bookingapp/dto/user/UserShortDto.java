package by.general.bookingapp.dto.user;

import by.general.bookingapp.dto.order.OrderFullDto;
import by.general.bookingapp.entity.UserRole;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class UserShortDto {

    private Integer id;

    private String firstName;

    private String lastName;

    private String phone;

}
