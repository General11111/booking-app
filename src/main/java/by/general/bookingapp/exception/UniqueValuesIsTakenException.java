package by.general.bookingapp.exception;

public class UniqueValuesIsTakenException extends RuntimeException{
    public UniqueValuesIsTakenException(String message) {
        super(message);
    }
}
