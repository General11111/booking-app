package by.general.bookingapp.exception;

public class AppEntityNotFoundException extends RuntimeException{
    public AppEntityNotFoundException(String message) {
        super(message);
    }
}
