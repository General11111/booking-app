package by.general.bookingapp.service.impl;

import by.general.bookingapp.dto.hotel.HotelCreateDto;
import by.general.bookingapp.dto.hotel.HotelFullDto;
import by.general.bookingapp.dto.hotel.HotelShortDto;
import by.general.bookingapp.dto.hotel.HotelUpdateDto;
import by.general.bookingapp.entity.CountryEntity;
import by.general.bookingapp.entity.HotelEntity;
import by.general.bookingapp.exception.AppEntityNotFoundException;
import by.general.bookingapp.mapper.HotelMapper;
import by.general.bookingapp.repository.CountryRepository;
import by.general.bookingapp.repository.HotelRepository;
import by.general.bookingapp.repository.RoomRepository;
import by.general.bookingapp.service.HotelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HotelServiceImpl implements HotelService {

    @Autowired
    HotelRepository hotelRepository;
    @Autowired
    HotelMapper hotelMapper;
    @Autowired
    CountryRepository countryRepository;
    @Autowired
    RoomRepository roomRepository;


    @Override
    public HotelFullDto create(HotelCreateDto createDto) {
        HotelEntity entityToCreate = hotelMapper.map(createDto);
        entityToCreate.setName(createDto.getName());
        entityToCreate.setAddress(createDto.getAddress());
        entityToCreate.setNumberOfStars(createDto.getNumberOfStars());
        CountryEntity countryEntity = countryRepository.getById(createDto.getCountryId());
        entityToCreate.setCountry(countryEntity);
        HotelEntity createdEntity = hotelRepository.save(entityToCreate);
        countryEntity.getHotels().add(createdEntity);
        HotelFullDto hotelFullDto = hotelMapper.map(createdEntity);
        return hotelFullDto;

    }

    @Override
    public HotelFullDto update(HotelUpdateDto updateDto) {
        HotelEntity existingEntity = hotelRepository.getById(updateDto.getId());
        if (existingEntity == null) {
            throw new AppEntityNotFoundException("HotelEntity was not found by id: " + updateDto.getId());
        }
        existingEntity.setName(updateDto.getName());
        existingEntity.setNumberOfStars(updateDto.getNumberOfStars());
        HotelFullDto hotelFullDto = hotelMapper.map(hotelRepository.save(existingEntity));
        System.out.println("Hotel was successfully updated.");
        return hotelFullDto;
    }

    @Override
    public HotelFullDto findById(int id) {
        HotelEntity foundEntity = hotelRepository.getById(id);
        if (foundEntity == null) {
            throw new AppEntityNotFoundException("HotelEntity was not found by id: " + id);
        }
        HotelFullDto hotelFullDto = hotelMapper.map(foundEntity);
        System.out.println("Hotel was successfully found.");
        return hotelFullDto;

    }

    @Override
    public List<HotelShortDto> findAll() {
        List<HotelEntity> foundEntities = hotelRepository.findAll();
        List<HotelShortDto> hotelShortDtos = hotelMapper.map(foundEntities);
        System.out.println(hotelShortDtos.size() + " hotels were found.");
        return hotelShortDtos;
    }

    @Override
    public List<HotelShortDto> findAllHotelsByCountry(Integer countryId) {
        List<HotelEntity> foundEntities = hotelRepository.findByCountry(countryId);
        List<HotelShortDto> hotelShortDtos = hotelMapper.map(foundEntities);
        System.out.println("Hotel was successfully found.");
        return hotelShortDtos;
    }

    @Override
    public void delete(int id) {
        HotelEntity foundEntity = hotelRepository.getById(id);
        if (foundEntity == null) {
            throw new AppEntityNotFoundException("HotelEntity was not found by id: " + id);
        }
        hotelRepository.delete(foundEntity);
        System.out.println("HotelServiceImpl -> Hotel was successfully deleted");
    }


}
