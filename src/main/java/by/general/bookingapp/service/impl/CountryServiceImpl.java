package by.general.bookingapp.service.impl;

import by.general.bookingapp.dto.country.CountryCreateDto;
import by.general.bookingapp.dto.country.CountryFullDto;
import by.general.bookingapp.dto.country.CountryUpdateDto;
import by.general.bookingapp.entity.CountryEntity;
import by.general.bookingapp.exception.AppEntityNotFoundException;
import by.general.bookingapp.mapper.CountryMapper;
import by.general.bookingapp.repository.CountryRepository;
import by.general.bookingapp.service.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;

@Service
public class CountryServiceImpl implements CountryService {

    @Autowired
    private CountryMapper countryMapper;

    @Autowired
    private CountryRepository countryRepository;

    @Override
    public CountryFullDto create(CountryCreateDto createDto) {
        CountryEntity entityToSave = countryMapper.map(createDto);
        CountryEntity savedEntity = countryRepository.save(entityToSave);
        CountryFullDto countryFullDto = countryMapper.map(savedEntity);
        System.out.println("CountryServiceImpl -> country was successfully created");
        return countryFullDto;
    }

    @Override
    public CountryFullDto update(CountryUpdateDto dto) {
        CountryEntity countryToUpdate = countryRepository.getById(dto.getId());
        if (countryToUpdate == null) {
            throw new AppEntityNotFoundException("CountryEntity was not found by id: " + dto.getId());
        }
        countryToUpdate.setName(dto.getName());
        CountryEntity updatedEntity = countryRepository.save(countryToUpdate);
        CountryFullDto countryFullDto = countryMapper.map(updatedEntity);
        System.out.println("CountryServiceImpl -> Country was successfully updated");
        return countryFullDto;
    }

    @Override
    public CountryFullDto findById(Integer id) {
        CountryEntity foundEntity = countryRepository.getById(id);
        if (foundEntity == null) {
            throw new AppEntityNotFoundException("CountryEntity was not found by id: " + id);
        }
        CountryFullDto countryFullDto = countryMapper.map(foundEntity);
        System.out.println("Country was successfully found.");
        return countryFullDto;
    }

    @Override
    public List<CountryFullDto> findAll() {
        List<CountryEntity> foundEntities = countryRepository.findAll();
        List<CountryFullDto> foundDtos = countryMapper.map(foundEntities);
        System.out.println(foundDtos.size() + " countries were found.");
        return foundDtos;
    }

    @Override
    public void delete(Integer id) {
        CountryEntity foundEntity = countryRepository.getById(id);
        if (foundEntity == null) {
            throw new AppEntityNotFoundException("CountryEntity was not found by id: " + id);
        }
        foundEntity.setDeletedAt(Instant.now());
        countryRepository.save(foundEntity);
        System.out.println("Country was successfully deleted.");
    }
}
