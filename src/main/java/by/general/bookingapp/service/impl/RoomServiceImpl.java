package by.general.bookingapp.service.impl;

import by.general.bookingapp.dto.room.RoomCreateDto;
import by.general.bookingapp.dto.room.RoomFullDto;
import by.general.bookingapp.dto.room.RoomShortDto;
import by.general.bookingapp.dto.room.RoomUpdateDto;
import by.general.bookingapp.entity.OrderEntity;
import by.general.bookingapp.entity.RoomEntity;
import by.general.bookingapp.entity.UserEntity;
import by.general.bookingapp.exception.AppEntityNotFoundException;
import by.general.bookingapp.mapper.RoomMapper;
import by.general.bookingapp.repository.RoomRepository;
import by.general.bookingapp.service.RoomService;
import by.general.bookingapp.service.config.ServiceConfig;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.time.Instant;
import java.time.LocalDate;
import java.util.List;

@Service
@RequiredArgsConstructor
public class RoomServiceImpl implements RoomService {

    private final RoomRepository roomRepository;
    private final RoomMapper roomMapper;
    private final ServiceConfig serviceConfig;


    @Override
    @Transactional
    public RoomFullDto findById(Integer id) {
        RoomEntity foundRoom = roomRepository.findById(id)
                .orElseThrow(() -> new AppEntityNotFoundException("RoomEntity was not found by id: " + id));
        RoomFullDto roomFullDto = roomMapper.map(foundRoom);
        System.out.println("Room was successfully found");
        return roomFullDto;

    }

    @Override
    @Transactional
    public List<RoomShortDto> findAll() {
        List<RoomEntity> foundRooms = roomRepository.findAll();
        List<RoomShortDto> roomShortDtos = roomMapper.map(foundRooms);
        System.out.println(roomShortDtos.size() + " rooms were found");
        return roomShortDtos;

    }

    @Override
    @Transactional
    public RoomFullDto create(RoomCreateDto roomCreateDto) {
        RoomEntity entityToSave = roomMapper.map(roomCreateDto);
        RoomEntity savedRoom = roomRepository.save(entityToSave);
        RoomFullDto roomFullDto = roomMapper.map(savedRoom);
        System.out.println("Room was successfully created");
        return roomFullDto;

    }

    @Override
    @Transactional
    public RoomFullDto update(RoomUpdateDto roomUpdateDto) {
        RoomEntity roomToUpdated = roomRepository.findById(roomUpdateDto.getId())
                .orElseThrow(() -> new AppEntityNotFoundException
                        ("RoomEntity was not found by id: " + roomUpdateDto.getId()));
        for (OrderEntity order : roomToUpdated.getOrders()) {
            if (order.getDateOut().after(Date.valueOf(LocalDate.now()))) {
                throw new RuntimeException("Cannot update the number, it still has unfulfilled orders");
            }
        }
        RoomEntity updatedRoom = roomRepository.save(roomToUpdated);
        RoomFullDto roomFullDto = roomMapper.map(updatedRoom);
        System.out.println("Room was successfully updated");
        return roomFullDto;

    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        RoomEntity roomToDeleted = roomRepository.findById(id)
                .orElseThrow(() -> new AppEntityNotFoundException("RoomEntity was not found by id: " + id));
        for (OrderEntity order : roomToDeleted.getOrders()) {
            UserEntity user = order.getUser();
            if (order.getDateIn().after(Date.valueOf(LocalDate.now()))) {
                user.setAmount(
                        user.getAmount() + (serviceConfig.calculateCostBooking(order.getDateIn(),
                                order.getDateOut(), order)));
            }
            order.setUser(user);
            order.setDeletedAt(Instant.now());
        }
        roomToDeleted.setDeletedAt(Instant.now());
        roomRepository.save(roomToDeleted);
        System.out.println("Room was successfully deleted");
    }
}
