package by.general.bookingapp.service.impl;

import by.general.bookingapp.dto.order.OrderCreateDto;
import by.general.bookingapp.dto.order.OrderFullDto;
import by.general.bookingapp.dto.order.OrderUpdateDto;
import by.general.bookingapp.entity.OrderEntity;
import by.general.bookingapp.entity.RoomEntity;
import by.general.bookingapp.entity.UserEntity;
import by.general.bookingapp.exception.AppEntityNotFoundException;
import by.general.bookingapp.mapper.OrderMapper;
import by.general.bookingapp.repository.OrderRepository;
import by.general.bookingapp.repository.RoomRepository;
import by.general.bookingapp.repository.UserRepository;
import by.general.bookingapp.service.OrderService;
import by.general.bookingapp.service.config.ServiceConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    OrderMapper orderMapper;
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    RoomRepository roomRepository;
    @Autowired
    ServiceConfig serviceConfig;

    @Override
    public OrderFullDto create(OrderCreateDto createDto) {

        serviceConfig.checkOrderDates(createDto.getDateIn(), createDto.getDateOut());

        OrderEntity orderToSave = orderMapper.map(createDto);

        orderToSave.setDateIn(createDto.getDateIn());
        orderToSave.setDateOut(createDto.getDateOut());
        orderToSave.setCost(createDto.getCost());

        RoomEntity room = roomRepository.getById(createDto.getRoomId());
        RoomEntity freeRoom = serviceConfig.findByFreeRoom(room, createDto);

        orderToSave.setRoom(freeRoom);

        UserEntity user = userRepository.getById(createDto.getUserId());

        Double orderCost = serviceConfig.calculateCostBooking(createDto.getDateIn(),
                createDto.getDateOut(), orderToSave);

        if (user.getAmount() > orderCost) {
            user.setAmount(user.getAmount() - orderCost);
            userRepository.save(user);
        } else {
            throw new RuntimeException("Boost your amount");
        }

        orderToSave.setUser(user);
        OrderEntity savedEntity = orderRepository.save(orderToSave);
        OrderFullDto orderFullDto = orderMapper.map(savedEntity);
        System.out.println("OrderServiceImpl -> user was successfully created");
        return orderFullDto;
    }

    @Override
    public OrderFullDto update(OrderUpdateDto updateDto) {
        serviceConfig.checkOrderDates(updateDto.getDateIn(), updateDto.getDateOut());
        OrderEntity orderToUpdate = orderRepository.getById(updateDto.getId());
        orderToUpdate.setDateIn(updateDto.getDateIn());
        orderToUpdate.setDateOut(updateDto.getDateOut());

        RoomEntity room = roomRepository.getById(updateDto.getRoomId());
        RoomEntity freeRoom = serviceConfig.findByFreeRoom(room, updateDto);

        orderToUpdate.setRoom(freeRoom);

        Double orderCost = serviceConfig.calculateCostBooking(updateDto.getDateIn(),
                updateDto.getDateOut(), orderToUpdate);

        orderToUpdate.setCost(orderCost);

        UserEntity user = orderToUpdate.getUser();
        orderToUpdate.setUser(user);

        return null;
    }

    @Override
    public OrderFullDto findById(int id) {
        OrderEntity foundOrder = orderRepository.getById(id);
        if (foundOrder == null) {
            throw new AppEntityNotFoundException("OrderEntity was not found by id: " + id);
        }
        OrderFullDto orderFullDto = orderMapper.map(foundOrder);
        System.out.println("OrderServiceImpl -> Order was successfully found");
        return orderFullDto;
    }

    @Override
    public List<OrderFullDto> findAll() {
        List<OrderEntity> foundOrders = orderRepository.findAll();
        List<OrderFullDto> orderFullDtos = orderMapper.map(foundOrders);
        System.out.println("OrderServiceImpl -> " + orderFullDtos.size() + " orders were found");
        return orderFullDtos;                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
    }

    @Override
    public void delete(Integer id) {
        OrderEntity orderToDeleted = orderRepository.getById(id);
        if (orderToDeleted == null) {
            throw new AppEntityNotFoundException("OrderServiceImpl -> OrderEntity was not found by id: " + id);
        }
        System.out.println("Country was successfully deleted.");
        orderToDeleted.setDeletedAt(Instant.now());
        orderRepository.save(orderToDeleted);
        System.out.println("Order was successfully deleted.");
    }

}
