package by.general.bookingapp.service.impl;

import by.general.bookingapp.dto.user.*;
import by.general.bookingapp.entity.OrderEntity;
import by.general.bookingapp.entity.UserEntity;
import by.general.bookingapp.exception.AppEntityNotFoundException;
import by.general.bookingapp.exception.UniqueValuesIsTakenException;
import by.general.bookingapp.exception.WrongUserPasswordException;
import by.general.bookingapp.mapper.UserMapper;
import by.general.bookingapp.repository.UserRepository;
import by.general.bookingapp.service.UserService;
import by.general.bookingapp.service.config.ServiceConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.time.Instant;
import java.time.LocalDate;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ServiceConfig serviceConfig;

    @Override
    public UserFullDto create(UserCreateDto createDto) {
        UserEntity entityToSave = userMapper.map(createDto);
        UserEntity entityWithSameEmail = userRepository.findByEmail(entityToSave.getEmail());
        if (entityWithSameEmail != null) {
            throw new UniqueValuesIsTakenException("Email is taken");
        }
        UserEntity savedEntity = userRepository.save(entityToSave);
        UserFullDto userFullDto = userMapper.map(savedEntity);
        System.out.println("UserServiceImpl -> user was successfully created");
        return userFullDto;
    }

    @Override
    public UserFullDto update(UserUpdateDto dto) {
        UserEntity userToUpdate = userRepository.getById(dto.getId());
        if (userToUpdate == null) {
            throw new AppEntityNotFoundException("UserEntity was not found by id: " + dto.getId());
        }
        userToUpdate.setFirstName(dto.getFirstName());
        userToUpdate.setLastName(dto.getLastName());
        userToUpdate.setPhone(dto.getPhone());
        UserEntity updatedUser = userRepository.save(userToUpdate);
        UserFullDto userFullDto = userMapper.map(updatedUser);
        System.out.println("UserServiceImpl -> User was successfully updated");
        return userFullDto;
    }

    @Override
    public UserFullDto findById(int id) {
        UserEntity foundUser = userRepository.getById(id);
        if (foundUser == null) {
            throw new AppEntityNotFoundException("UserEntity was not found by id: " + id);
        }
        UserFullDto userFullDto = userMapper.map(foundUser);
        System.out.println("UserServiceImpl -> User was successfully found");
        return userFullDto;
    }

    @Override
    public List<UserShortDto> findAll() {
        List<UserEntity> foundUsers = userRepository.findAll();
        List<UserShortDto> userShortDtos = userMapper.map(foundUsers);
        System.out.println("UserServiceImpl -> " + userShortDtos.size() + " users were found");
        return userShortDtos;
    }

    @Override
    public void delete(int id) {
        UserEntity userToDelete = userRepository.getById(id);
        if (userToDelete == null) {
            throw new AppEntityNotFoundException("UserEntity was not found by id: " + id);
        }
        for (OrderEntity order : userToDelete.getOrders()) {
            if (order.getDateIn().after(Date.valueOf(LocalDate.now()))) {
                userToDelete.setAmount(userToDelete.getAmount()
                        + serviceConfig.calculateCostBooking(order.getDateIn(), order.getDateOut(), order));
            }
            order.setUser(userToDelete);
            order.setDeletedAt(Instant.now());
        }
        userToDelete.setDeletedAt(Instant.now());
        userRepository.save(userToDelete);
        System.out.println("UserServiceImpl -> User was successfully deleted");
    }

    @Override
    public void changePassword(ChangeUserPasswordDto dto) {
        UserEntity userToUpdate = userRepository.getById(dto.getId());
        if (userToUpdate == null) {
            throw new AppEntityNotFoundException("UserEntity was not found by id: " + dto.getId());
        }
        if (!userToUpdate.getPassword().equals(dto.getOldPassword())) {
            throw new WrongUserPasswordException("Wrong password!");
        }
        userToUpdate.setPassword(dto.getNewPassword());
        userRepository.save(userToUpdate);

        System.out.println("UserServiceImpl -> User password was successfully updated");
    }

    @Override
    public UserFullDto changeRole(ChangeUserRoleDto dto) {
        UserEntity userToUpdate = userRepository.getById(dto.getId());
        if (userToUpdate == null) {
            throw new AppEntityNotFoundException("UserEntity was not found by id: " + dto.getId());
        }

        userToUpdate.setRole(dto.getNewRole());
        
        UserEntity updatedUser = userRepository.save(userToUpdate);
        UserFullDto userDto = userMapper.map(updatedUser);
        return userDto;
    }

    @Override
    public void changeAmount(ChangeAmountDto dto) {
        UserEntity userToUpdate = userRepository.getById(dto.getId());
        if (userToUpdate == null) {
            throw new AppEntityNotFoundException("UserEntity was not found by id: " + dto.getId());
        }
        if (!userToUpdate.getPassword().equals(dto.getPassword()) && !userToUpdate.getEmail().equals(dto.getEmail())) {
            throw new WrongUserPasswordException("Wrong password & email!");
        }
        userRepository.changeAmount(userToUpdate.getAmount(), userToUpdate.getId());
        System.out.println("UserServiceImpl -> User amount was successfully updated");
    }
}
