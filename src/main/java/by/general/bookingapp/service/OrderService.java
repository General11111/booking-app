package by.general.bookingapp.service;

import by.general.bookingapp.dto.order.OrderCreateDto;
import by.general.bookingapp.dto.order.OrderFullDto;
import by.general.bookingapp.dto.order.OrderUpdateDto;

import java.util.List;

public interface OrderService {

    OrderFullDto create(OrderCreateDto dto);

    OrderFullDto update(OrderUpdateDto dto);

    OrderFullDto findById(int id);

    List<OrderFullDto> findAll();

    void delete(Integer id);

}
