package by.general.bookingapp.service;

import by.general.bookingapp.dto.country.CountryCreateDto;
import by.general.bookingapp.dto.country.CountryFullDto;
import by.general.bookingapp.dto.country.CountryUpdateDto;
import by.general.bookingapp.dto.user.UserFullDto;

import java.util.List;

public interface CountryService {


    CountryFullDto create(CountryCreateDto dto);

    CountryFullDto update(CountryUpdateDto dto);

    CountryFullDto findById(Integer id);

    List<CountryFullDto> findAll();

    void delete(Integer id);

}
