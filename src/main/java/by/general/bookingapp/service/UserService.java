package by.general.bookingapp.service;

import by.general.bookingapp.dto.user.*;

import java.util.List;

public interface UserService {

    UserFullDto create(UserCreateDto dto);

    UserFullDto update(UserUpdateDto dto);

    UserFullDto findById(int id);

    List<UserShortDto> findAll();

    void delete(int id);

    void changePassword(ChangeUserPasswordDto dto);

    UserFullDto changeRole(ChangeUserRoleDto dto);

    void changeAmount(ChangeAmountDto dto);

}
