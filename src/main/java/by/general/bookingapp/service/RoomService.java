package by.general.bookingapp.service;

import by.general.bookingapp.dto.room.RoomCreateDto;
import by.general.bookingapp.dto.room.RoomFullDto;
import by.general.bookingapp.dto.room.RoomShortDto;
import by.general.bookingapp.dto.room.RoomUpdateDto;

import java.util.List;

public interface RoomService {

    RoomFullDto findById(Integer id);

    List<RoomShortDto> findAll();

    RoomFullDto create(RoomCreateDto roomCreateDto);

    RoomFullDto update(RoomUpdateDto roomUpdateDto);

    void deleteById(Integer id);


}
