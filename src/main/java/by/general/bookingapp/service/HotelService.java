package by.general.bookingapp.service;

import by.general.bookingapp.dto.hotel.HotelCreateDto;
import by.general.bookingapp.dto.hotel.HotelFullDto;
import by.general.bookingapp.dto.hotel.HotelShortDto;
import by.general.bookingapp.dto.hotel.HotelUpdateDto;
import by.general.bookingapp.dto.room.RoomShortDto;

import java.util.List;

public interface HotelService {

    HotelFullDto create(HotelCreateDto dto);

    HotelFullDto update(HotelUpdateDto dto);

    HotelFullDto findById(int id);

    List<HotelShortDto> findAll();

    List<HotelShortDto> findAllHotelsByCountry(Integer countryId);

    void delete(int id);

}
