package by.general.bookingapp.service.config;

import by.general.bookingapp.dto.order.OrderCreateDto;
import by.general.bookingapp.dto.order.OrderUpdateDto;
import by.general.bookingapp.entity.OrderEntity;
import by.general.bookingapp.entity.RoomEntity;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

@Service
public class ServiceConfig {

    public Double calculateCostBooking(Date startDate, Date endDate, OrderEntity order) {
        long millisecondsInOneDay = 86400000;
        int daysOfBooking = (int) ((endDate.getTime() - startDate.getTime()) / millisecondsInOneDay + 1);
        return order.getCost() * daysOfBooking;
    }

    public void checkOrderDates(Date dateIn, Date dateOut) {
        if (dateIn.before(Date.valueOf(LocalDate.now()))) {
            throw new RuntimeException("Booking start date cannot be earlier than now");
        }
        if (dateIn.after(dateOut)) {
            throw new RuntimeException("The start date of the booking cannot be later than the end date of the booking");
        }
    }

    public RoomEntity findByFreeRoom(RoomEntity roomEntity, OrderCreateDto createDto) {
        List<OrderEntity> orders = roomEntity.getOrders();
        for (OrderEntity order : orders) {
            if (order.getDateIn().after(createDto.getDateOut()) || order.getDateOut().before(createDto.getDateIn())) {
                return roomEntity;
            }
        }
        throw new RuntimeException("There are no available rooms for the given dates in this hotel");
    }

    public RoomEntity findByFreeRoom(RoomEntity roomEntity, OrderUpdateDto updateDto) {
        List<OrderEntity> orders = roomEntity.getOrders();
        for (OrderEntity order : orders) {
            if (order.getDateIn().after(updateDto.getDateOut()) || order.getDateOut().before(updateDto.getDateIn())) {
                return roomEntity;
            }
        }
        throw new RuntimeException("There are no available rooms for the given dates in this hotel");
    }


}
