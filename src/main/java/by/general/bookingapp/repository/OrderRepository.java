package by.general.bookingapp.repository;

import by.general.bookingapp.entity.OrderEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface OrderRepository extends JpaRepository<OrderEntity, Integer> {

    @Query(value = "SELECT * FROM orders WHERE user_id = :userId AND deleted_at IS NULL", nativeQuery = true)
    OrderEntity findByUser(@Param("userId") Integer userId);

    @Query(value = "SELECT * FROM orders JOIN rooms ON rooms.id = orders.room_id \n" +
            "WHERE rooms.hotel_id = :hotelId AND orders.deleted_at IS NULL;", nativeQuery = true)
    List<OrderEntity> findByHotel(@Param("hotelId") Integer hotelId);

    @Query(value = "SELECT * FROM orders WHERE room_id = :roomId AND deleted_at IS NULL", nativeQuery = true)
    List<OrderEntity> findByRoom(@Param("roomId")Integer roomId);

}
