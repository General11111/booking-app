package by.general.bookingapp.repository;

import by.general.bookingapp.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserRepository extends JpaRepository<UserEntity, Integer> {

    UserEntity findByEmail(String email);

    @Query(value = "SELECT * FROM users WHERE role = 'ADMIN'", nativeQuery = true)
    List<UserEntity> findAllAdmins();

    @Modifying
    @Query(value = "UPDATE users SET amount =:sum WHERE id =:userId AND deleted_at IS NULL ", nativeQuery = true)
    void changeAmount(@Param("sum") Double sum, @Param("userId") Integer userId);



}
