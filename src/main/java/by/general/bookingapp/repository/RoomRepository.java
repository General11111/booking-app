package by.general.bookingapp.repository;

import by.general.bookingapp.entity.RoomEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RoomRepository extends JpaRepository<RoomEntity, Integer> {

    @Query(value = "SELECT * FROM rooms WHERE hotel_id=:hotelId AND number_of_places=:number AND deleted_at IS NULL", nativeQuery = true)
    List<RoomEntity> findByHotelAndNumberOfPlaces(@Param("hotelId") Integer hotelId, @Param("number") Integer numberOfPeople);

    @Query(value = "SELECT * FROM rooms WHERE hotel_id=:hotelId AND deleted_at IS NULL", nativeQuery = true)
    List<RoomEntity> findAllRoomsByHotel(@Param("hotelId") Integer hotelId);






}
