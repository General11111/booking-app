package by.general.bookingapp.mapper;

import by.general.bookingapp.dto.user.UserFullDto;
import by.general.bookingapp.entity.UserEntity;
import by.general.bookingapp.entity.UserRole;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UserMapperTest {

    @Autowired
    private UserMapper userMapper;

    @Test
    public void mapToDto_happyPath() {

        // given
        UserEntity entity = new UserEntity();
        entity.setId(15);

        entity.setFirstName("Alla");
        entity.setLastName("Alla");
        entity.setPhone("+375 29 6768892");
        entity.setEmail("alla@mail.ru");
        entity.setPassword("12345678");
        entity.setRole(UserRole.ADMIN);
        entity.setAmount(200.54);
        // when
        UserFullDto dto = userMapper.map(entity);

        // then
        Assertions.assertNotNull(dto);
        System.out.println("->>>>>" + dto);

    }

}
