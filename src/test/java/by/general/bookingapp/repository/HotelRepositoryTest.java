package by.general.bookingapp.repository;

import by.general.bookingapp.entity.HotelEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class HotelRepositoryTest {

    @Autowired
    private HotelRepository hotelRepository;

    @Test
    public void findByCountryTest() {
        List<HotelEntity> hotels = hotelRepository.findByCountry(2);
        System.out.println(hotels);
    }

}
