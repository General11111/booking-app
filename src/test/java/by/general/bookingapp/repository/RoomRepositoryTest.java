package by.general.bookingapp.repository;

import by.general.bookingapp.entity.RoomEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class RoomRepositoryTest {

    @Autowired
    private RoomRepository roomRepository;

    @Test
    public void findByHotelAndNumberOfPlacesTest() {
        List<RoomEntity> findRooms = roomRepository.findByHotelAndNumberOfPlaces(1, 3);
        System.out.println(findRooms);
    }
    @Test
    public void findAllRoomsByHotel() {
        List<RoomEntity> findAllRooms = roomRepository.findAllRoomsByHotel(2);
        System.out.println(findAllRooms);
    }

}
