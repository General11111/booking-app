package by.general.bookingapp.repository;

import by.general.bookingapp.entity.UserEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@SpringBootTest
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    @Transactional
    public void testChangeAmount() {
        userRepository.changeAmount(100.22, 1);
    }

    @Test
    @Transactional
    public void testFindAllAdmins() {
        List<UserEntity> findAdmins = userRepository.findAllAdmins();
        System.out.println(findAdmins);
    }

}
