package by.general.bookingapp.repository;

import by.general.bookingapp.entity.OrderEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import java.util.List;

@SpringBootTest
public class OrderRepositoryTest {

    @Autowired
    private OrderRepository orderRepository;

    @Test
    public void findByUserTest() {
        OrderEntity orderEntity = orderRepository.findByUser(3);
        System.out.println(orderEntity);
    }

    @Test
    public void findByHotelTest() {
        List<OrderEntity> orderEntity = orderRepository.findByHotel(2);
        System.out.println(orderEntity);
    }

    @Test
    public void findByRoomTest() {
        List<OrderEntity> orderEntity = orderRepository.findByRoom(1);
        System.out.println(orderEntity);
    }

}
