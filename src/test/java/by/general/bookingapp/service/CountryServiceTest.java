package by.general.bookingapp.service;

import by.general.bookingapp.dto.country.CountryCreateDto;
import by.general.bookingapp.dto.country.CountryFullDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static by.general.bookingapp.BookingappApplicationTests.FAKER;

@SpringBootTest
public class CountryServiceTest {

    @Autowired
    private CountryService countryService;

    @Test
    public void create_happyPath() {

        CountryCreateDto createDto = generateCountryCreateDto();
        CountryFullDto created = countryService.create(createDto);
        Assertions.assertNotNull(created);

    }

    public CountryCreateDto generateCountryCreateDto() {
        CountryCreateDto countryCreateDto = new CountryCreateDto();
        countryCreateDto.setName(FAKER.address().country());
        return countryCreateDto;
    }

}
